import 'dart:convert';
import 'package:cometa/Models/imageModel.dart';
import 'package:http/http.dart' as http;
import 'package:cometa/Consts/consts.dart';

class ImageRepository {
  Future<List<ImageModel>> imagesList() async {
    final response = await http.get(Uri.parse(Consts.hostName + Consts.images));
    print(json.decode(response.body));
    return ImageResponse.fromJsonList(json.decode(response.body.toString()))
        .images;
  }
}
